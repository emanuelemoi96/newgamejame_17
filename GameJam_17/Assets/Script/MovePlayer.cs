﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MovePlayer : MonoBehaviour {

	public Rigidbody2D body;
	public float speed;
	public Vector2 endPos;

	void Start () {

		body = GetComponent<Rigidbody2D> ();

	}

	void Update () {

		if (Moving () == false) 
		{
			
		}
	}

	bool Moving()
	{
		float step = speed * Time.deltaTime;
		body.position =  Vector2.MoveTowards(body.position, new Vector2(endPos.x, body.position.y), step);

		if (body.position.x == endPos.x) 
		{
			return false;
		}

		return true;
	}
}
