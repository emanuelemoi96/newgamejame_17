﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : MonoBehaviour {

	public static string P1CustomName;
	public static string P2CustomName;
	public static string P1CustomAge;
	public static string P2CustomAge;
	public static string P1CustomJob;
	public static string P2CustomJob;
	public static string P1CustomHate;
	public static string P2CustomHate;
	public static string P1CustomGoal;
	public static string P2CustomGoal;
	public static string P1CustomInterest;
	public static string P2CustomInterest;

	public static GameManager GameManagerRef;

	void Awake()
	{
		if (GameManagerRef != null) 
		{
			GameObject.Destroy (gameObject);
		} 
		else 
		{
			GameObject.DontDestroyOnLoad (gameObject);
			GameManagerRef = this;
		}
	}

	void Start () {
		
	}

	void Update () {
		
	}
}
