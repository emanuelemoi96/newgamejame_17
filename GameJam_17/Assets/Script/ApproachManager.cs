﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ApproachManager : MonoBehaviour {

	public static float Love = 0.3f;
	public static bool P1Turn = false;
	public static bool P2Turn = false;
	public bool bGameStart = false;

	public Image loveBar;
	public Rigidbody2D bodyP1;
	public SpriteRenderer spriteP1;
	public Animator animP2;
	public Animator animP1;
	public SpriteRenderer spriteP2;
	public Rigidbody2D bodyP2;
	public Rigidbody2D finalBody;
	public SpriteRenderer finalSprite;
	public Canvas MainCanvas;
	public float speed;

	public Animator finalAnim;

	public Vector2 endPosP1;
	public Vector2 endPosP2;



	void Start () {
		MainCanvas = GameObject.Find ("Canvas").GetComponent<Canvas> ();
		spriteP2 = GameObject.Find ("P2").GetComponent<SpriteRenderer> ();
		spriteP1 = GameObject.Find ("P1").GetComponent<SpriteRenderer> ();
		finalSprite = GameObject.Find ("Final").GetComponent<SpriteRenderer> ();
		finalBody = GameObject.Find ("Final").GetComponent<Rigidbody2D> ();
		finalAnim = GameObject.Find ("Final").GetComponent<Animator> ();
		animP1 = GameObject.Find ("P1").GetComponent<Animator> ();
		animP2 = GameObject.Find ("P2").GetComponent<Animator> ();
		loveBar = GameObject.Find ("Love").GetComponent<Image> ();
		bodyP1 = GameObject.Find ("P1").GetComponent<Rigidbody2D> ();
		bodyP2 = GameObject.Find ("P2").GetComponent<Rigidbody2D> ();
	}

	void Update()
	{
		if (loveBar) 
		{
			if (Love <= 0.05f) 
			{
				MainCanvas.enabled = false;
				float step = speed * Time.deltaTime;
				bodyP2.position = Vector2.MoveTowards (bodyP2.position, new Vector2 (12.54f, bodyP2.position.y), step);
			}
			loveBar.fillAmount = Love;
				if (Love >= 0.6f) 
				{
					animP1.SetBool ("Sitting",true);
					animP2.SetBool ("Sitting",true);

					if (Love >= 1f) 
					{
						MainCanvas.enabled = false;
						
						spriteP1.enabled = false;
						spriteP2.enabled = false;
						finalSprite.enabled = true;
						finalAnim.SetBool ("Ending", true);

						float step = speed * Time.deltaTime;
						finalBody.position = Vector2.MoveTowards (finalBody.position, new Vector2 (12.54f, finalBody.position.y), step);
					}
				}
		}
	}

	void FixedUpdate () 
	{

		Moving ();
	}
		
	void Moving()
	{
		if (bodyP1 && bodyP2 && Love > 0.05f) 
		{
			float step = speed * Time.deltaTime;
			bodyP1.position = Vector2.MoveTowards (bodyP1.position, new Vector2 (endPosP1.x, bodyP1.position.y), step);
			bodyP2.position = Vector2.MoveTowards (bodyP2.position, new Vector2 (endPosP2.x, bodyP2.position.y), step);

			if (bodyP1.position.x == endPosP1.x && bodyP2.position.x == endPosP2.x && bGameStart == false) {
				P1Turn = true;
				bGameStart = true;
			}
		}
	}
}
