﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class P1Manager : MonoBehaviour {

	public Text mainPhrase;
	public List <string> sentences;
	public int stageIndx;

	public Text choice1;
	public Text choice2;
	public Text choice3;

	public string playerName;
	public string playerAge;
	public string playerInterest;
	public string playerJob;
	public string playerHate;
	public string playerGoal;


	void Start () 
	{
		playerName = GameManager.P1CustomName;
		playerAge = GameManager.P1CustomAge;
		playerInterest = GameManager.P1CustomInterest;
		playerJob = GameManager.P1CustomJob;
		playerHate = GameManager.P1CustomHate;
		playerGoal = GameManager.P1CustomGoal;

		CheckForInfo ();

		stageIndx = 1;
	}

	void Update () 
	{
		if (ApproachManager.P1Turn == true) 
		{
			switch (stageIndx) 
			{
			case 1:
				Stage (0, 1, 2);
				break;
			case 2:
				Stage (3, 4, 5);
				break;
			case 3:
				Stage (6, 7, 8);
				break;
			case 4:
				Stage (9, 10, 11);
				break;
			case 5:
				Stage (12, 13, 14);
				break;
			case 6:
				Stage (15, 16, 17);
				break;
			case 7:
				Stage (18, 19, 20);
				break;
			}
		}
	}

	void Stage(int s1, int s2, int s3)
	{
		mainPhrase.text = "";
		
		choice1.text = "1." + sentences [s1];
		choice2.text = "2." + sentences [s2];
		choice3.text = "3." + sentences [s3];

		if (Input.GetKeyDown (KeyCode.Alpha1)) 
		{
			mainPhrase.text = sentences [s1];
			clearChoice ();
			stageIndx++;
			ApproachManager.P2Turn = true;
			ApproachManager.P1Turn = false;
		}
		if (Input.GetKeyDown (KeyCode.Alpha2)) 
		{
			mainPhrase.text = sentences [s2];
			clearChoice ();
			stageIndx++;
			ApproachManager.P2Turn = true;
			ApproachManager.P1Turn = false;
		}
		if (Input.GetKeyDown (KeyCode.Alpha3))
		{
			mainPhrase.text = sentences [s3];
			clearChoice ();
			stageIndx++;
			ApproachManager.P2Turn = true;
			ApproachManager.P1Turn = false;
		} 
	}

	void clearChoice()
	{
		choice1.text = "";
		choice2.text = "";
		choice3.text = "";
	}

	void CheckForInfo()
	{
		for (int i = 0; i < sentences.Count; i++) 
		{
			if (sentences [i].Contains ("@name")) 
			{
				sentences [i]= sentences [i].Replace ("@name", playerName);
			}
			if (sentences [i].Contains ("@age")) 
			{
				sentences [i]= sentences [i].Replace ("@age", playerAge);
			}
			if (sentences [i].Contains ("@job")) 
			{
				sentences [i]= sentences [i].Replace ("@job", playerJob);
			}
			if (sentences [i].Contains ("@interest")) 
			{
				sentences [i]= sentences [i].Replace ("@interest", playerInterest);
			}
			if (sentences [i].Contains ("@hate")) 
			{
				sentences [i]= sentences [i].Replace ("@hate", playerHate);
			}
			if (sentences [i].Contains ("@goal")) 
			{
				sentences [i]= sentences [i].Replace ("@goal", playerGoal);
			}
		}
	}
}
