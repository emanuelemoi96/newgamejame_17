﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class MenuManager : MonoBehaviour {

	public GameObject start;
	public Button confirm;

	public InputField P1NamePrefab;
	public InputField P2NamePrefab;
	public InputField P1JobPrefab;
	public InputField P2JobPrefab;
	public InputField P1AgePrefab;
	public InputField P2AgePrefab;
	public InputField P1InterestPrefab;
	public InputField P2InterestPrefab;
	public InputField P1HatePrefab;
	public InputField P2HatePrefab;
	public InputField P1GoalPrefab;
	public InputField P2GoalPrefab;

	public SpriteRenderer P1;
	public SpriteRenderer P2;

	private InputField P1Name;
	private InputField P2Name;
	private InputField P1Job;
	private InputField P2Job;
	private InputField P1Age;
	private InputField P2Age;
	private InputField P1Hate;
	private InputField P2Hate;
	private InputField P1Interest;
	private InputField P2Interest;
	private InputField P1Goal;
	private InputField P2Goal;

	void Start () 
	{
		
		//P1 = transform.FindChild ("personaggio1").GetComponent<Image> ();
		//P2 = transform.FindChild ("personaggio2").GetComponent<Image> ();

	}

	void Update () 
	{
		
	}

	public void startCustomize()
	{
		P1.enabled = true;
		P2.enabled = true;
		confirm.image.enabled = true;
		P1Name = Instantiate (P1NamePrefab, transform);
		P2Name = Instantiate (P2NamePrefab, transform);
		P1Job = Instantiate (P1JobPrefab, transform);
		P2Job = Instantiate (P2JobPrefab, transform);
		P1Age = Instantiate (P1AgePrefab, transform);
		P2Age = Instantiate (P2AgePrefab, transform);
		P1Interest = Instantiate (P1InterestPrefab, transform);
		P2Interest = Instantiate (P2InterestPrefab, transform);
		P1Hate = Instantiate (P1HatePrefab, transform);
		P2Hate = Instantiate (P2HatePrefab, transform);
		P1Goal = Instantiate (P1GoalPrefab, transform);
		P2Goal = Instantiate (P2GoalPrefab, transform);

		Destroy (start);
	}

	public void startGame()
	{
		GameManager.P1CustomName = P1Name.text;
		GameManager.P2CustomName = P2Name.text;
		GameManager.P1CustomAge = P1Age.text;
		GameManager.P2CustomAge = P2Age.text;
		GameManager.P1CustomJob = P1Job.text;
		GameManager.P2CustomJob = P2Job.text;
		GameManager.P1CustomGoal = P1Goal.text;
		GameManager.P2CustomGoal = P2Goal.text;
		GameManager.P1CustomHate = P1Hate.text;
		GameManager.P2CustomHate = P2Hate.text;
		GameManager.P1CustomInterest = P1Interest.text;
		GameManager.P2CustomInterest = P2Interest.text;

		SceneManager.LoadScene ("Main");
	}
}
