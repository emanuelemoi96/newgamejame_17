﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class P2Manager : MonoBehaviour {

	public Text mainPhrase;
	public List <string> sentences;
	public int stageIndx;
	public bool bHaveToJudge = true;

	public Text choice1;
	public Text choice2;
	public Text choice3;

	public string playerName;
	public string playerAge;
	public string playerInterest;
	public string playerJob;
	public string playerHate;
	public string playerGoal;

	void Start () 
	{
		stageIndx = 1;

		playerName = GameManager.P2CustomName;
		playerAge = GameManager.P2CustomAge;
		playerInterest = GameManager.P2CustomInterest;
		playerJob = GameManager.P2CustomJob;
		playerHate = GameManager.P2CustomHate;
		playerGoal = GameManager.P2CustomGoal;

		CheckForInfo ();
	}

	void Update () {

		if (ApproachManager.P2Turn == true && bHaveToJudge == true) 
		{
			Judge();
		} 
		else if (ApproachManager.P2Turn == true && bHaveToJudge == false) 
		{
			switch (stageIndx) 
			{
			case 1:
				Stage (0, 1, 2);
				break;
			case 2:
				Stage (3, 4, 5);
				break;
			case 3:
				Stage (6, 7, 8);
				break;
			case 4:
				Stage (9, 10, 11);
				break;
			case 5:
				Stage (12, 13, 14);
				break;
			case 6:
				Stage (15, 16, 17);
				break;
			case 7:
				Stage (18, 19, 20);
				break;
			}
		}
	}

	void Stage(int s1, int s2, int s3)
	{
		choice1.text = "7." + sentences [s1];
		choice2.text = "8." + sentences [s2];
		choice3.text = "9." + sentences [s3];

		if (Input.GetKeyDown (KeyCode.Alpha7)) 
		{
			mainPhrase.text = sentences [s1];
			clearChoice ();
			stageIndx++;
			bHaveToJudge = true;
			ApproachManager.P1Turn = true;
			ApproachManager.P2Turn = false;
		}
		if (Input.GetKeyDown (KeyCode.Alpha8)) 
		{
			mainPhrase.text = sentences [s2];
			clearChoice ();
			stageIndx++;
			bHaveToJudge = true;
			ApproachManager.P1Turn = true;
			ApproachManager.P2Turn = false;
		}
		if (Input.GetKeyDown (KeyCode.Alpha9)) 
		{
			mainPhrase.text = sentences [s3];
			clearChoice ();
			stageIndx++;
			bHaveToJudge = true;
			ApproachManager.P1Turn = true;
			ApproachManager.P2Turn = false;
		}
	}

	void Judge()
	{
		mainPhrase.text = "Hai Apprezzato?";

		choice1.text = "7.Si";
		choice2.text = "8.No";

		if (Input.GetKeyDown (KeyCode.Alpha7)) 
		{
			mainPhrase.text = "";
			ApproachManager.Love += 0.1f;
			bHaveToJudge = false;
			if (ApproachManager.Love <= 1f && stageIndx == 7) 
			{
				ApproachManager.Love = 0f;
			}
		}
		if (Input.GetKeyDown (KeyCode.Alpha8)) 
		{
			mainPhrase.text = "";
			ApproachManager.Love -= 0.1f;
			bHaveToJudge = false;
			if (ApproachManager.Love <= 1f && stageIndx == 7) 
			{
				ApproachManager.Love = 0f;
			}
		}
	}

	void clearChoice()
	{
		choice1.text = "";
		choice2.text = "";
		choice3.text = "";
	}

	void CheckForInfo()
	{
		for (int i = 0; i < sentences.Count; i++) 
		{
			if (sentences [i].Contains ("@name")) 
			{
				sentences [i]= sentences [i].Replace ("@name", playerName);
			}
			if (sentences [i].Contains ("@age")) 
			{
				sentences [i]= sentences [i].Replace ("@age", playerAge);
			}
			if (sentences [i].Contains ("@job")) 
			{
				sentences [i]= sentences [i].Replace ("@job", playerJob);
			}
			if (sentences [i].Contains ("@interest")) 
			{
				sentences [i]= sentences [i].Replace ("@interest", playerInterest);
			}
			if (sentences [i].Contains ("@hate")) 
			{
				sentences [i]= sentences [i].Replace ("@hate", playerHate);
			}
			if (sentences [i].Contains ("@goal")) 
			{
				sentences [i]= sentences [i].Replace ("@goal", playerGoal);
			}
		}
	}
}
